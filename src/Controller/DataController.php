<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DataController extends AbstractController
{
    public function globalData(): array {
        return [
            'controller_name' => 'HomeController',
            'personal_name' => 'José',
            'persons_array' => [
                "Nelle",
                "José",
                "Cyril"
            ],
            'items' => [
                'Home',
                'View',
                'List',
                'Form'
            ]
        ];
    }
}
